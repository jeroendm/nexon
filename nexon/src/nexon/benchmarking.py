#!/usr/bin/env python
import rospy
import rospkg
import subprocess
from nexon_msgs.srv import PTPPlanning, PTPPlanningRequest
from nexon_msgs.srv import LINPlanning, LINPlanningRequest
from geometry_msgs.msg import Pose, Vector3, Quaternion


class BenchmarkRunner:
    def __init__(self, setup_name):
        self.main_pkg = 'benchmark_runner'
        self.setup_name = setup_name
        self.support_pkg = setup_name + '_support'
        self.moveit_pkg = setup_name + '_moveit_config'

        self.setup = subprocess.Popen(['roslaunch', self.moveit_pkg, 'demo.launch'],
                                      stdout=subprocess.PIPE,
                                      stderr=subprocess.STDOUT)

    def load_task(self, task_name):
        subprocess.call(
            ['rosrun', self.main_pkg, 'publish_work.py', self.support_pkg, task_name])

    def run_task(self, task_name):
        subprocess.call(['rosrun', self.main_pkg, 'simple_task.py'])


class Task:
    def __init__(self, name):
        self.name = name
        self.defs, self.commands = self.read_from_parameter_server()
        print(self.defs)
        for com in self.commands:
            print(com)

    def read_from_parameter_server(self):
        definitions = rospy.get_param(self.name + "/variables")
        commands = rospy.get_param(self.name + "/commands")
        return definitions, commands


class PlannerSpecificRunner:
    def __init__(self, service_name, planner_name):
        self.service_name = service_name
        self.planner_name = planner_name
        print(service_name, planner_name)
        rospy.wait_for_service(self.service_name, timeout=1.0)
        self.planning_service = rospy.ServiceProxy(
            self.service_name, PTPPlanning)

        self.rospack = rospkg.RosPack()
        self.filepath = ""

    def pose_to_msg(self, pose):
        pos = pose["xyz"]
        quat = pose["xyzw"]
        pos_msg = Vector3(x=pos[0], y=pos[1], z=pos[2])
        quat_msg = Quaternion(
            x=quat[0], y=quat[1], z=quat[2], w=quat[3])
        return Pose(position=pos_msg, orientation=quat_msg)

    def send_pose_goal(self, pose_msg):
        req = PTPPlanningRequest()
        req.pose_goal = pose_msg
        req.planner = self.planner_name
        resp = self.planning_service(req)
        return resp

    def send_joint_goal(self, joint_values):
        req = PTPPlanningRequest()
        req.joint_goal = joint_values
        req.planner = self.planner_name
        resp = self.planning_service(req)
        return resp

    # def send_lin_pose_goal(self, pose_msg, con_msg):
    #     req = LINPlanningRequest()
    #     req.pose_goal = pose_msg
    #     req.pose_constraint = con_msg
    #     req.start_config = []  # insert current config here

    def write_results_to_file(self, results):
        pkg_path = self.rospack.get_path("benchmark_runner")
        filepath = pkg_path + "/data/benchmark_1_results.csv"
        print("Writing results to " + filepath)
        self.filepath = filepath

        header = ",".join("j" + str(i) for i in range(7)) + "\n"

        with open(filepath, "w") as file:
            file.write("segment1,segment2,segment3,length1,length2.lenght3\n")
            file.write(",".join(str(v) for v in results["success"]) + ",")
            file.write(",".join(str(len(pt))
                                for pt in results["plans"]) + "\n")
            file.write(header)
            for plan in results["plans"]:
                for point in plan:
                    file.write(",".join(str(v)
                                        for v in list(point.positions)) + "\n")

    def run(self, task):
        print("Running task with service " + self.service_name +
              " and planner " + self.planner_name)

        results = {"success": [], "plans": []}
        for i, step in enumerate(task.commands):
            print("Calculating plan for step " + str(i))
            if step["type"] == "movej":
                res = self.send_joint_goal(task.defs[step["goal"]])
                results["success"].append(res.success)
                if res.success:
                    results["plans"].append(res.joint_path)
            elif step["type"] == "movep":
                goal_pose = self.pose_to_msg(task.defs[step["goal"]])
                res = self.send_pose_goal(goal_pose)
                results["success"].append(res.success)
                if res.success:
                    results["plans"].append(res.joint_path)
            else:
                rospy.logerr("Unkown task step type: " + step["type"])

        print(results["success"])
        self.write_results_to_file(results)
        if False in results["success"]:
            return False, ""
        else:
            return True, self.filepath


def create_runners(parameter_name):
    settings = rospy.get_param(parameter_name)
    runners = []
    for s in settings:
        runners.append(PlannerSpecificRunner(
            s["planning_service_name"], s["planner_name"]))
    return runners
