"""
This module describes and implements the task representation.

Out of the two options, using classes and namedtuples, or using
a dictionary, I opted for the latter.
This is easier to convert to a text based format such as json.
More flexible maybe, I'm not sure yet.

This module contains string variables that
specify the file format used to describe
planning tasks.
"""


class Commands:
    MOVEP = "movep"
    MOVEJ = "movej"
    MOVELIN = "movel"
    SET_REFERENCE = "set-reference"


class Sections:
    VARS = "variables"
    COMMANDS = "commands"
    CONSTRAINTS = "constraints"

    @classmethod
    def types(cls):
        """ Return all available section strings.

        Hardcoded for now, let's find a way to fix this.
        d = cls.__dict__
        return [key for key in d if not key.startswith("__")]
        """
        return ["variables", "commands", "constraints"]
