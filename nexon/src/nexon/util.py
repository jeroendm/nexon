import json
import rospkg
import numpy as np

import rospy
import geometry_msgs.msg
import rviz_tools_py.rviz_tools


def tuples_to_pose(pos, ori):
    """ Covert (x,y,z) position and (x,y,z,w) quaternion to geometry pose message."""
    p = geometry_msgs.msg.Pose()
    p.orientation.x = ori[0]
    p.orientation.y = ori[1]
    p.orientation.z = ori[2]
    p.orientation.w = ori[3]
    p.position.x = pos[0]
    p.position.y = pos[1]
    p.position.z = pos[2]
    return p


class Plotter:
    """
    Wrapper around rviz_tools_py to visualise things.
    No idea how te remove a specific marker instead of
    all markers. (to remove opject when it is picked by the robot,
    but keep pick frame axis visible)
    """

    def __init__(self, topic_name="/visual_markers", ref_frame="/world", wait_time=None):
        self.rvt = rviz_tools_py.rviz_tools.RvizMarkers(
            ref_frame, topic_name, wait_time=wait_time)
        # give publisher some time to get ready (0.2 to short)
        rospy.sleep(0.5)
        # todo there not realy a quick and easy way to remove the arbitrary 0.5 number
        self.rvt.deleteAllMarkers()

    def plot_object(self, pose):
        """ Plot and add object mesh (or cube in comments) and axis"""
        # plot simple cube
        # scale = geometry_msgs.msg.Vector3(0.08, 0.08, 0.01)
        # self.rvt.publishCube(pose, 'red', scale)

        mesh_file = "package://setup_2_support/meshes/small_wobj.stl"
        mesh_pose = geometry_msgs.msg.Pose()
        mesh_scale = geometry_msgs.msg.Vector3(1.0, 1.0, 1.0)
        self.rvt.publishMesh(pose, mesh_file, 'red', mesh_scale)
        self.rvt.publishAxis(pose, 0.1, 0.01)

    def plot_axis(self, pose):
        self.rvt.publishAxis(pose, 0.1, 0.01)

    def plot_named_axis(self, pose, text):
        self.rvt.publishAxis(pose, 0.1, 0.01)
        self.rvt.publishText(pose, text, 'black',
                             geometry_msgs.msg.Vector3(0.1, 0.1, 0.1))

    def delete_all_markers(self):
        self.rvt.deleteAllMarkers()


def write_plans_to_file(plans):
    """
    Write a list of plans to a simple csv file with columns
    time, j1, j2, j3, ..., v1, v2, v3, ..., a1, a2, a3
    """
    jp = []
    jv = []
    ja = []
    time = []
    offset = 0.0
    for plan in plans:
        for pt in plan.joint_trajectory.points:
            jp.append(pt.positions)
            jv.append(pt.velocities)
            ja.append(pt.accelerations)
            time.append(pt.time_from_start.to_sec() + offset)
        offset = time[-1]

    jp = np.array(jp)
    jv = np.array(jv)
    ja = np.array(ja)
    time = np.array(time).reshape(len(time), 1)
    print(jp.shape, jv.shape, ja.shape, time.shape)

    data = np.hstack((time, jp, jv, ja))
    print(data.shape)
    rospack = rospkg.RosPack()
    filepath = rospack.get_path('benchmark_runner') + \
        '/config/pick_and_place_output_path.txt'
    np.savetxt(filepath, data, delimiter=',')
