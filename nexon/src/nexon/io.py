import rospy
import pymongo
import numpy as np

from tf.transformations import quaternion_from_euler, quaternion_from_matrix, euler_matrix, quaternion_matrix
from nexon.interface import Commands, Sections


class LogDB:
    """ Interface for mongodb database to organize adding and editing data. """

    def __init__(
            self,
            host='localhost',
            port=27017,
            name="benchmark",
            collection="runner"
    ):
        self.client = pymongo.MongoClient(host, port)
        self.db = self.client[name]
        self.col = collection

    def add_data(self, data):
        return self.db[self.col].insert_one(data)


def strs_to_floats(lst):
    return [float(e) for e in lst]


def transform_to_dict(transform):
    """ Convert 4x4 transform matrix to dict with
    {"xyz": [x, y, z], "xyzw"; [qx, qy, qz, qw]}
    """
    if isinstance(transform, list):
        # this is not a transform but a list of joint values
        return transform

    translation = list(transform[:3, 3])
    quaternion = quaternion_from_matrix(transform)
    return {"xyz": translation, "xyzw": quaternion}


def parse_file(filepath):
    """ Parse a txt file with the industrial robot like
    task specification.

    There are three sections:
    - variables
    - commands
    - constraints
    """
    with open(filepath) as file:
        data = file.readlines()
        output = {
            Sections.VARS: {},
            Sections.COMMANDS: [],
            Sections.CONSTRAINTS: {}
        }
        output[Sections.VARS]["default"] = np.eye(4)

        current_key = None
        current_ref = output[Sections.VARS]["default"]
        for line in data:
            line = line.rstrip()
            if line in Sections.types():
                current_key = line
                continue
            if current_key is None:
                rospy.logerr(
                    "File did not start with 'variables' or 'commands'.")
            if line == "":
                continue
            if Commands.SET_REFERENCE in line:
                _, ref = line.split(" ")
                try:
                    current_ref = output[Sections.VARS][ref]
                    continue
                except KeyError:
                    rospy.logerr(
                        "Trying to set reference to unkown pose: {}".format(
                            ref)
                    )

            if current_key == Sections.VARS:
                vtype, name, value = parse_variable(line, current_ref)
                output[current_key][name] = value
            elif current_key == Sections.COMMANDS:
                output[current_key].append(parse_command(line))
            elif current_key == Sections.CONSTRAINTS:
                ctype, name, lower, upper = parse_constraint(line)
                output[current_key][name] = {
                    "type": ctype, "min": lower, "max": upper}

    output[Sections.VARS] = {k: transform_to_dict(
        v) for k, v in output[Sections.VARS].items()}

    return output


def guess_degree_or_radians(values):
    """ Assume that for an average larger than 2*pi, the values where given in degrees. """
    TRESHOLD = 2 * np.pi
    not_zero = [abs(v) for v in values if v != 0]
    if len(not_zero) == 0:
        # all values are zero, the guess does not mather
        return "rad"
    elif (sum(not_zero) / len(not_zero) > TRESHOLD):
        return "deg"
    else:
        return "rad"


def parse_rotation(values):
    """ Converts a list to a quaternion list [x, y, z, w].

    If the list has a lenght of 3 it is converted to euler angles using
    the relative XYZ angles.
    It tries to guess wheter the angles supplied are given in radians or degrees.
    """
    if len(values) == 4:
        return quaternion_matrix(values)
    elif len(values) == 3:
        if guess_degree_or_radians(values) == "rad":
            return euler_matrix(values[0], values[1], values[2], axes="rxyz")
        else:
            values = [v * np.pi / 180 for v in values]
            return euler_matrix(values[0], values[1], values[2], axes="rxyz")
    else:
        raise ValueError(
            "Wrong number of values for the rotation: {}".format(str(values)))


def parse_variable(line, reference_frame):
    """
    Assign pose or joint values to variable names.
        config <name> <q1> <q2> ...
        pose <name> <x> <y> <z> <qx> <qy> <qz> <qw>
    For a pose, premultiply with the reference frame in wich
    it is expressed.
    """
    # print("Parsing var line: {}".format(line))
    v = line.split(" ")
    assert len(v) >= 3
    vtype = v[0]
    name = v[1]
    if vtype == "config":
        value = [float(e) for e in v[2:]]
    elif vtype == "pose":
        all_values = [float(e) for e in v[2:]]
        value = parse_rotation(all_values[3:])
        value[:3, 3] = all_values[:3]
        value = np.dot(reference_frame, value)
    else:
        rospy.logerr("Unkown variable type " + vtype)
    return vtype, name, value


def parse_command(line):
    """
    Motion commands using named configs / poses and constraints.
        movep <goal>
        movej <goal>
        movelin <goal>
        movecirc <goal>
    After all of the above commands we can add path constraints
        movelin <goal> constraints c1
    TODO: also allow goal constraints, not only path constraints.
    """
    # print("Parsing command line: {}".format(line))
    v = line.split(" ")
    assert len(v) >= 2
    if len(v) == 2:
        return {"type": v[0], "goal": v[1]}
    else:
        assert len(v) >= 4
        assert v[2] == "constraints"
        return {"type": v[0], "goal": v[1], "constraints": v[3:]}


def parse_constraint(line):
    """
    Constraints section:
        xyz <name> symmetric <x> <y> <z>
        xyz <name> minmax <xmin> <ymin> <zmin> <xmax> <ymax> <zmax>
        rpy <name> symmetric <r> <p> <y>
        rpy <name> minmax <rmin> <pmin> <ymin> <rmax> <pmax> <ymax>
    """
    # print("Parsing con line: {}".format(line))
    v = line.split(" ")
    assert len(v) >= 5
    ctype, name, bounds = v[0], v[1], v[2]
    if bounds == "symmetric":
        upper = strs_to_floats(v[3:])
        lower = [-e for e in upper]
    elif bounds == "minmax":
        lower = strs_to_floats(v[3:6])
        upper = strs_to_floats(v[6:])

    return ctype, name, lower, upper
