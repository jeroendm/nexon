#!/usr/bin/env python
import json
import rospkg

from nexon.io import parse_file


rospack = rospkg.RosPack()


def test_pick_task():
    fp1 = rospack.get_path("nexon") + "/test/pick_task_1.txt"
    fp2 = rospack.get_path("nexon") + "/test/pick_task_1.json"

    data = parse_file(fp1)
    with open(fp2) as file:
        desired_data = json.load(file)

    assert data == desired_data


def test_weld_task():
    fp1 = rospack.get_path("nexon") + "/test/welding_1.txt"
    fp2 = rospack.get_path("nexon") + "/test/welding_1.json"

    data = parse_file(fp1)
    print(data)

    with open(fp2) as file:
        desired_data = json.load(file)

    assert data == desired_data


if __name__ == "__main__":
    test_pick_task()
    test_weld_task()
