#!/usr/bin/env python
from __future__ import print_function

import sys
import unittest
import time
import rospy
import rostest
import geometry_msgs.msg
import visualization_msgs.msg

import nexon.util

PKG = 'nexon'
NAME = 'util_test'


class TestDummy(unittest.TestCase):
    def __init__(self, *args):
        super(TestDummy, self).__init__(*args)
        self.marker_published = False
        self.plt = nexon.util.Plotter()
        self.marker_ns = []
        # we expect a mesh and axis (x, y and z)
        self.desired_marker_ns = ["Mesh", "Cylinder", "Cylinder", "Cylinder"]

    def create_pose(self):
        pose = geometry_msgs.msg.Pose()
        pose.orientation = geometry_msgs.msg.Quaternion(0, 0, 0, 1)
        pose.position = geometry_msgs.msg.Vector3(0, 0, 0)
        return pose

    def publish_marker(self):
        self.plt.plot_object(self.create_pose())

    def callback(self, data):
        print("Marker message received!")
        print(data)
        self.marker_ns.append(data.ns)
        print(self.marker_ns)
        if self.marker_ns == self.desired_marker_ns:
            self.marker_published = True

    def test_plot_object(self):
        rospy.Subscriber('/setup_2_marker',
                         visualization_msgs.msg.Marker, self.callback)
        time.sleep(0.1)
        self.publish_marker()

        timeout_t = time.time() + 10.0*1000
        while not rospy.is_shutdown() and not self.marker_published and time.time() < timeout_t:
            time.sleep(0.1)
        self.assert_(self.marker_published,
                     "Timed out before receiving Marker message.")


if __name__ == '__main__':
    rospy.init_node('util-test-node')
    rostest.rosrun(PKG, NAME, TestDummy, sys.argv)
