cmake_minimum_required(VERSION 2.8.3)
project(nexon)

## Compile as C++11, supported in ROS Kinetic and newer
# add_compile_options(-std=c++11)

## Find catkin macros and libraries
## if COMPONENTS list like find_package(catkin REQUIRED COMPONENTS xyz)
## is used, also find other catkin packages
find_package(catkin REQUIRED COMPONENTS
  rospy
)

## Uncomment this if the package has a setup.py. This macro ensures
## modules and global scripts declared therein get installed
## See http://ros.org/doc/api/catkin/html/user_guide/setup_dot_py.html
catkin_python_setup()

catkin_package(
#  INCLUDE_DIRS include
#  LIBRARIES nexon
#  CATKIN_DEPENDS geometry_msgs message_generation moveit_commander rospkg rospy rviz_tools_py std_msgs std_srvs trajectory_msgs
#  DEPENDS system_lib
)


#############
## Testing ##
#############
## Add folders to be run by python nosetests
# catkin_add_nosetests(test)
find_package(rostest)
add_rostest(test/test_util.launch)
